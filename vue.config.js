module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? 'https://pawnya.gitlab.io/simtech-test/'
    : '/'
}